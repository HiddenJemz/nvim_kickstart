# nvim_kickstart



## Getting started

This is the neovim kickstart from [TJ_Devries](https://github.com/nvim-lua/kickstart.nvim) from [Twitch](https://www.twitch.tv/teej_dv) and [Youtube](https://www.youtube.com/@teej_dv).\
Follow the [Tutorial](https://www.youtube.com/watch?v=m8C0Cq9Uv9o&ab_channel=TJDeVries) carefully.\
Enjoy the Neovim journey!!!
